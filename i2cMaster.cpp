#include "i2cMaster.h"

void setupTwiTimer(void) {
	TCCR1A = 0x00;
	//TCCR1A |= (1<<WGM11) | (1<<WGM10) //Set none

	TCCR1B = 0x00;
	TCCR1B |= (1<<WGM12); //Sets timer to ctc when other wgm bits are 0x00

	TCCR1B |= (1<<CS10); //no prescaler

	OCR1AH = 0x00; //Dont count to the 8 highest bits of the 16bit counter value.
	OCR1AL = 0x28; //Compare when counter is 0x01.

	TIMSK1 = 0x00;
	TIMSK1 |= (1<<OCIE1A); //Enable compare B

	TIFR1 = 0x00;
	TIFR1 |= (1<<OCF1A);
}

ISR(TIMER1_COMPA_vect){
		PINB = LED_PIN;
}