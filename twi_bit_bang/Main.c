// main
// use the atstk200 starter kit and external prototype board to program
// an Atmel AT17C010 device. Leds on PORTC are used for visual status indicators
// 4.7k ohm pull-up resistors are used on SDA and SCL

#include "at17c.h"

volatile unsigned char t0_timed_out;
volatile unsigned char t1_timed_out;
unsigned char wrbuf[PAGE_SIZE];
unsigned char rdbuf[PAGE_SIZE];

extern void WritePage(unsigned int address, unsigned char *bufptr);
extern void ReadPage(unsigned int address, unsigned char *bufptr);
void ProgramResetPolarity(unsigned char state);
unsigned char VerifyResetPolarity(void);
extern void initTwi(void);

void C_task main(void) {

unsigned int address = 0x20;
unsigned char i;

    initTwi();

    _SEI(); // enable interrupts

    while (1) {
 
        for (; address < 0x28; address++) {
            WritePage(address,&wrbuf[0]);
//            ReadPage(address,&rdbuf[0]);
            
            
            ProgramResetPolarity(0);
          
            ProgramResetPolarity(0xff);            
        }    
        
    } // while (1)

} // main
