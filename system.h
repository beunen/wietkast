//Pin defines
#define F_CPU 8000000UL				//Clock speed 8Mhz
#define LED_PIN 0x01				//PB0 (pin 0) on PORTB
#define GLOBAL_INTERRUPT_MASK 0x80	//bit 7 in SREG Status Register. Enables interrupts to work.
#define __AVR_ATtiny841__

#include <avr/io.h>
#include <avr/interrupt.h>

