// Includes
#include "system.h"
#include "main.h"

int main(void)
{
	//setupBlinkTimer();
	setupTwiTimer();
	
	sei();
	
	PORTB |= LED_PIN;
	DDRB |= LED_PIN;
	
	
		
    while (1) {
//		_delay_ms(500);
//		PINB = LED_PIN; //Toggles bit.
    }
}

#define NORMAL_PORT_OPERATION_AC0A_DISCONNECTED_MASK = 0b11000000 //Disconnects pin from timer
#define WGM_NORMAL_MASK_TCCR0A = 0x00000011; //Clearing these bits sets counter to normal mode.
#define WGM_NORMAL_MASK_TCCR0B = 0x00001000; //Clearing these bits sets counter to normal mode.
#define TOP 0xFF; //TOP value of counter0
#define MAX 0xFF; //MAX value of counter0
#define BLINK_COMPARE_MATCH 63 //When the timer has matched 63 times the timer will have been running for 63 * 0.0008sec = 0.504 sec's

// test
uint16_t blink_timer_match_counter = 0;

void setupBlinkTimer(void) {
	//Set counter to 0.
	blink_timer_match_counter = 0;
	
	//Set compare Output Mode to non-PWM mode
	TCCR0A &= ~(1 << COM0A1);
	TCCR0A &= ~(1 << COM0A0);
	
	//Waveform generation mode = normal. //TOP = MAX = 0xFF.
	TCCR0A &= ~(1 << WGM00);
	TCCR0A &= ~(1 << WGM01);
	TCCR0B &= ~(1 << WGM02);

	//Configure Timer to use clk/256 prescaler. Results in 31250 ticks per second.
	TCCR0B &= ~(1 << CS00);
	TCCR0B &= ~(1 << CS01);
	TCCR0B |= (1 << CS02);
	
	//Set Match compare on Output Compare Register A
	OCR0A = 250; //250 will result in the compare to happen 125 per second. or every 0.008 seconds.
	
	//Enable interrupt on Timer/Counter0 Output Compare Match A Interrupt Enable.
	TIMSK0 |= (1 << OCIE0A);
}


//#define TIM_0_COMPA 0x0009

//Triggers every 0.008 seconds. 125Hz
ISR(TIMER0_COMPA_vect){
	blink_timer_match_counter++;
	if (blink_timer_match_counter >= BLINK_COMPARE_MATCH){
		blink_timer_match_counter = 0;		//Reset timer match counter for next compare.
	
		PINB = LED_PIN; //Toggles bit.
	}
}