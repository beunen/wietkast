#ifndef _MAIN_H_

#define _MAIN_H_

// Includes
#include "system.h"
#include "i2cMaster.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// Prototypes
void setupBlinkTimer(void);
void setupTwiTimer(void);

#endif
