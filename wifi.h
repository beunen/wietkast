#ifndef _WIFI_H_

#define _WIFI_H_

#include "system.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void setupTwiTimer(void);

#endif