#ifndef _LCD_H_

#define _LCD_H_

#include "system.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void setupTwiTimer(void);

#endif