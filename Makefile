CC=gcc
CXX=avr-g++
RM=rm -f
CPPFLAGS=-g
LDFLAGS=-g
LDLIBS=$(shell -libs)

SRCS := $(shell find . -maxdepth 1 -name "*.cpp")
OBJS  := $(patsubst %.cpp, %.o, $(SRCS))

all: wietkast

wietkast: $(OBJS)
	$(CXX) -o wietkast.elf $(OBJS) -mmcu=attiny841 -B "\attiny841"

	avr-objcopy.exe -O ihex -R .eeprom -R .fuse -R .lock -R .signature -R .user_signatures  "Wietkast.elf" "Wietkast.hex"
	avr-objcopy.exe -j .eeprom  --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0  --no-change-warnings -O ihex "Wietkast.elf" "Wietkast.eep" || exit 0
	avr-objdump.exe -h -S "Wietkast.elf" > "Wietkast.lss"
	avr-objcopy.exe -O srec -R .eeprom -R .fuse -R .lock -R .signature -R .user_signatures "Wietkast.elf" "Wietkast.srec"
	avr-size.exe "Wietkast.elf"

burn: wietkast
	avrdude.exe -c usbasp -p t841 -B12 -U flash:w:"Wietkast.hex"

clean:
	$(RM) $(OBJS)

dist-clean: clean
	$(RM) wietkast