#ifndef _WATCHDOG_H_

#define _WATCHDOG_H_

#include "system.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void setupTwiTimer(void);

#endif